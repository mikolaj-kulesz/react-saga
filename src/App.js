import React, {useCallback, useEffect, useState} from 'react';
import {useSelector, useDispatch} from 'react-redux';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import Container from '@material-ui/core/Container';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Avatar from '@material-ui/core/Avatar';
import IconButton from '@material-ui/core/IconButton';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import LabelIcon from '@material-ui/icons/Label';
import DeleteIcon from '@material-ui/icons/Delete';
import './App.css';
import * as classes from './App.module.scss';
import moment from 'moment';
import * as actions from './store/actions/index';
import 'moment/locale/pl';  // without this line it didn't work
moment.locale('pl');

const App = () => {

    // Redux store
    const reduxItems = useSelector(state => state.itemsStore.items);
    const dispatch = useDispatch();
    const reduxSetItems = useCallback(() => {
        dispatch(actions.setItems());
    }, [dispatch]);
    const reduxAddItem = useCallback((newItem) => {
        dispatch(actions.addItem(newItem));
    }, [dispatch]);
    const reduxRemoveItem = useCallback((id) => {
        dispatch(actions.removeItem(id));
    }, [dispatch]);

    // useState
    const [name, setName] = useState('');
    const [isItemValid, setIsItemValid] = useState(false);

    // methods
    const addNewItem = async (e) => {
        e.preventDefault();
        if (!name) return;
        reduxAddItem({
            name,
        });
        resetInputField();
    };
    const removeItem = async (id) => reduxRemoveItem(id);
    const resetInputField = () => setName('');

    // useEffects
    useEffect(() => {
        reduxSetItems();
    }, [reduxSetItems]);

    useEffect(() => {
        if (name && name.length > 3) {
            setIsItemValid(true);
            return;
        }
        setIsItemValid(false);
    }, [name]);

    // markup
    const itemElements = reduxItems.map(item => {
        const dateFormatted = moment(item.date).format('LLLL');
        return (
            <ListItem
                key={item.id}
                className={classes.list_item}>
                <ListItemAvatar>
                    <Avatar>
                        <LabelIcon/>
                    </Avatar>
                </ListItemAvatar>
                <ListItemText
                    primary={item.name}
                    secondary={dateFormatted}/>
                <ListItemSecondaryAction
                    onClick={() => removeItem(item.id)}>
                    <IconButton
                        edge="end"
                        aria-label="delete">
                        <DeleteIcon/>
                    </IconButton>
                </ListItemSecondaryAction>
            </ListItem>
        );
    });

    return (
        <div>
            <CssBaseline/>
            <Container
                maxWidth="lg"
                style={{
                    backgroundColor: '#dfdfdf',
                    padding: 20,
                }}
            >
                <section>
                    <Typography
                        component="h1"
                        color="textPrimary"
                        variant="h2"
                        style={{
                            padding: [0, 0, 20, 0],
                        }}>
                        Add New Item:
                    </Typography>
                    <form
                        onSubmit={addNewItem}
                        noValidate
                        autoComplete="off">
                        <div
                            style={{
                                padding: [20, 20, 20, 0],
                                maxWidth: '30%',
                            }}>
                            <TextField
                                id="standard-basic"
                                fullWidth
                                label="Item name"
                                value={name}
                                onChange={(e) => setName(e.target.value)}
                                style={{
                                    marginBottom: 20,
                                }}/>
                            <Button
                                type="submit"
                                fullWidth
                                variant="contained"
                                disabled={!isItemValid}
                                size="large"
                                color="primary"
                            >Add item</Button>
                        </div>
                    </form>
                </section>

            </Container>

            <Container
                maxWidth="lg"
                style={{
                    padding: 20,
                }}
            >
                <List
                    component="nav"
                    aria-label="todo list"
                >
                    {itemElements}
                </List>
            </Container>
        </div>
    );
};

export default App;
