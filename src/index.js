import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';

// redux related
import {createStore, combineReducers, compose, applyMiddleware} from 'redux';
import {Provider} from 'react-redux';
import itemsReducer from './store/reducers/items';
import {logger} from './store/middleware';
import reduxThunk from 'redux-thunk';
import createSagaMiddleware from 'redux-saga';
import {watchItems} from './store/sagas/index';

const sagaMiddleWare = createSagaMiddleware();

const rootReducer = combineReducers({
    itemsStore: itemsReducer,
});
const middleware = applyMiddleware(logger, reduxThunk, sagaMiddleWare);
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(rootReducer, composeEnhancers(middleware));
ReactDOM.render(<Provider store={store}><App/></Provider>, document.getElementById('root'));

sagaMiddleWare.run(watchItems);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();


