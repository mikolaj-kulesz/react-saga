import {put, call} from 'redux-saga/effects';
import itemService from '../../services/ItemService';
import {setItems, setItemsHandler} from '../actions';

export function* addItemSaga(action) {
    yield itemService.addItem(action.newItem);
    yield put(setItems());
}

export function* removeItemSaga(action) {
    yield itemService.deleteItem(action.itemIdentifier);
    yield call([itemService, 'deleteItem'], action.itemIdentifier);
    yield put(setItems());
}

export function* setItemsSaga() {
    const newItems = yield itemService.getItems();
    const newItemsArr = yield Object.keys(newItems).map(key => {
        return {
            ...newItems[key],
            id: key,
        };
    }).reverse();
    yield put(setItemsHandler(newItemsArr));
}
