import {takeEvery, all} from 'redux-saga/effects';
import * as actionTypes from '../actions/actionTypes';
import {addItemSaga, removeItemSaga, setItemsSaga} from './items';

export function* watchItems() {
    yield all([
        takeEvery(actionTypes.ADD_ITEM_INIT, addItemSaga),
        takeEvery(actionTypes.REMOVE_ITEM_INIT, removeItemSaga),
        takeEvery(actionTypes.SET_ITEMS_INIT, setItemsSaga),
    ]);
}
