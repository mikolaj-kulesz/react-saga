export const SET_ITEMS = 'SET_ITEMS';
export const SET_ITEMS_INIT = 'SET_ITEMS_INIT';
export const ADD_ITEM = 'ADD_ITEM';
export const ADD_ITEM_INIT = 'ADD_ITEM_INIT';
export const REMOVE_ITEM = 'REMOVE_ITEM';
export const REMOVE_ITEM_INIT = 'REMOVE_ITEM_INIT';
