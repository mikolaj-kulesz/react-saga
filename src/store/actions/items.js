import * as actionTypes from './actionTypes'
// import itemService from '../../services/ItemService';

export const setItems = () => {
    return {
        type: actionTypes.SET_ITEMS_INIT,
    };
};

export const setItemsHandler = (value) => {
    return {
        type: actionTypes.SET_ITEMS,
        value,
    };
};

export const addItem = (newItem) => {
    return {
        type: actionTypes.ADD_ITEM_INIT,
        newItem,
    };
}

export const removeItem = (itemIdentifier) => {
    return {
        type: actionTypes.REMOVE_ITEM_INIT,
        itemIdentifier,
    };
};

// ===============================================
// handlers

