import * as actionTypes from '../actions/actionTypes'

const initialState = {
    items: []
}

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.SET_ITEMS:
            return {
                ...state,
                items: [...action.value]
            }
        default: return state
    }
}

export default reducer
