const to = async (promise) => {
    try {
        const res = await promise;
        return {
            error: null,
            res,
        };
    } catch (error) {
        return {
            error
        };
    }
};

export default to
