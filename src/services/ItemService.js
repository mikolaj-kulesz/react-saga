import to from '../utils/await-to-promise';
import globals from '../globals/globals';

class ItemService {

    async getItems() {
        const {itemsUrl} = globals
        const {error, res} = await to(fetch(`${itemsUrl}/items.json`).then(res => res.json()));
        if (error) return {};
        return (res) || {};
    }

    async addItem(itemData) {
        const {itemsUrl} = globals
        const {name} = itemData;
        const {error} = await to(fetch(`${itemsUrl}/items.json`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                name,
                date: new Date(),

            }),
        }));
        if (error) return error;
        return true;
    };

    async deleteItem(itemIdentifier) {
        const {itemsUrl} = globals
        const {error} = await to(fetch(`${itemsUrl}/items/${itemIdentifier}.json`, {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json',
            },
        }));
        return !error;
    };
}

export default new ItemService()
